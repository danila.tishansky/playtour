FROM adoptopenjdk/openjdk11:alpine

ARG JAR_FILE=build/libs/playtour-0.0.1-SNAPSHOT.jar

COPY ${JAR_FILE} playtour.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","playtour.jar"]