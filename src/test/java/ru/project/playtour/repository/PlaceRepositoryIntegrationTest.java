package ru.project.playtour.repository;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.JdbcDatabaseContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.project.playtour.entity.Place;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
@SpringBootTest
public class PlaceRepositoryIntegrationTest {

    @Autowired
    private PlaceRepository placeRepository;

    @Container
    public static JdbcDatabaseContainer postgreSQLContainer = new PostgreSQLContainer("postgres:14.0-alpine")
            .withDatabaseName("playtour")
            .withUsername("postgres")
            .withPassword("root")
            .withInitScript("db/init-test-db.sql");

    @DynamicPropertySource
    static void postgresqlProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        registry.add("spring.flyway.enabled", () -> false);
    }

    @Test
    void allContainersIsRunning() {
        assertTrue(postgreSQLContainer.isRunning());
    }

    @Test
    void shouldSavePlace() {
        var place = new Place();
        place.setName("name");
        place.setPrice(1);
        place.setMapIcon("some icon");
        place.setExpValue(12);
        place.setMoneyValue(11);
        place.setLongitude(1.2f);
        place.setLatitude(1.3f);
        place.setDescription("some description");
        assertEquals(place, placeRepository.save(place));
        Assertions.assertEquals(place.getName(), placeRepository.getById(1L).getName());
    }


}