create table place
(
    id          bigserial primary key,
    latitude    real         not null,
    longitude   real         not null,
    name        varchar(64)  not null,
    description text         null,
    map_icon    varchar(255) not null,
    price       int          not null default 0,
    exp_value   int          not null default 0,
    money_value int          not null default 0
);

create table place_photo
(
    id       bigserial primary key,
    place_id bigint references place (id) on delete cascade on update cascade,
    path     varchar(255)
);

create table visited_place
(
    user_id  varchar not null,
    place_id bigint not null references place (id) on delete cascade on update cascade,
    primary key (user_id, place_id)
);

create table tour
(
    id          bigserial primary key,
    name        varchar(64) not null,
    description text        null,
    exp_value   int         not null default 0,
    money_value int         not null default 0
);

create table place_in_tour
(
    tour_id  bigint not null references tour (id) on delete cascade on update cascade,
    place_id bigint not null references place (id) on delete cascade on update cascade,
    primary key (tour_id, place_id)
);

alter table public.visited_place add column date date;
