package ru.project.playtour.mapper;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import ru.project.playtour.dto.PlaceDto;
import ru.project.playtour.entity.Place;

import java.util.List;

@Mapper
public interface PlaceMapper {

    CycleAvoidingMappingContext CONTEXT = new CycleAvoidingMappingContext();

    PlaceDto toDto(Place entity, @Context CycleAvoidingMappingContext context);

    Place toEntity(PlaceDto dto, @Context CycleAvoidingMappingContext context);

    List<PlaceDto> toDto(List<Place> entityList, @Context CycleAvoidingMappingContext context);
}
