package ru.project.playtour.mapper;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import ru.project.playtour.dto.TourDto;
import ru.project.playtour.entity.Tour;

import java.util.List;

@Mapper
public interface TourMapper {
    CycleAvoidingMappingContext CONTEXT = new CycleAvoidingMappingContext();

    TourDto toDto(Tour entity, @Context CycleAvoidingMappingContext context);

    Tour toEntity(TourDto dto, @Context CycleAvoidingMappingContext context);

    List<TourDto> toDto(List<Tour> entities, @Context CycleAvoidingMappingContext context);

}
