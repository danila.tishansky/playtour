package ru.project.playtour.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.project.playtour.dto.VisitedPlaceDto;
import ru.project.playtour.entity.VisitedPlace;

import java.util.List;

@Mapper
public interface VisitedPlaceMapper {

    @Mapping(source = "dto.place.id", target = "id.placeId")
    @Mapping(source = "dto.userId", target = "id.userId")
    VisitedPlace toEntity(VisitedPlaceDto dto);

    @Mapping(source = "entity.id.userId", target = "userId")
    VisitedPlaceDto toDto(VisitedPlace entity);

    List<VisitedPlaceDto> toDto(List<VisitedPlace> entityList);

}
