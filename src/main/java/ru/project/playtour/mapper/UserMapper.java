package ru.project.playtour.mapper;

import org.keycloak.representations.idm.UserRepresentation;
import org.mapstruct.Mapper;
import ru.project.playtour.dto.Role;
import ru.project.playtour.dto.UserAttribute;
import ru.project.playtour.dto.UserDto;
import ru.project.playtour.exception.InvalidAttributeException;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Mapper
public interface UserMapper {

    default UserRepresentation toRepresentation(UserDto user) {
        var userRepresentation = new UserRepresentation();
        userRepresentation.setUsername(user.getUsername());
        userRepresentation.setEnabled(true);
        userRepresentation.setRealmRoles(Collections.singletonList(Role.USER.toString()));
        userRepresentation.setAttributes(Map.of(
                UserAttribute.PHOTO.getAttribute(), Collections.singletonList(user.getPhoto()),
                UserAttribute.LEVEL.getAttribute(), Collections.singletonList(String.valueOf(user.getLevel())),
                UserAttribute.EXPERIENCE.getAttribute(), Collections.singletonList(String.valueOf(user.getExperience())),
                UserAttribute.MONEY.getAttribute(), Collections.singletonList(String.valueOf(user.getMoney()))));
        return userRepresentation;
    }

    default UserDto toDto(UserRepresentation userRepresentation) {
        var user = new UserDto();
        user.setId(userRepresentation.getId());
        user.setUsername(userRepresentation.getUsername());
        user.setExperience(Integer.parseInt(getAttribute(userRepresentation, UserAttribute.EXPERIENCE.getAttribute())));
        user.setPhoto(getAttribute(userRepresentation, UserAttribute.PHOTO.getAttribute()));
        user.setMoney(Integer.parseInt(getAttribute(userRepresentation, UserAttribute.MONEY.getAttribute())));
        user.setLevel(Integer.parseInt(getAttribute(userRepresentation, UserAttribute.LEVEL.getAttribute())));
        return user;
    }

    default String getAttribute(UserRepresentation userRepresentation, String attribute) {
        var userAttributes = Optional.ofNullable(userRepresentation.getAttributes())
                .orElseThrow(InvalidAttributeException::new);
        return Optional.ofNullable(userAttributes.get(attribute).get(0))
                .orElseThrow(() -> new InvalidAttributeException(attribute));
    }

    default List<UserDto> toDto(List<UserRepresentation> userRepresentations) {
        return userRepresentations.stream().map(this::toDto).collect(Collectors.toList());
    }

}
