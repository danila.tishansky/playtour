package ru.project.playtour;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlayTourApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlayTourApplication.class, args);

    }

}
