package ru.project.playtour.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.project.playtour.entity.VisitedPlace;
import ru.project.playtour.entity.VisitedPlaceKey;

import java.util.List;

@Repository
public interface VisitedPlaceRepository extends JpaRepository<VisitedPlace, VisitedPlaceKey> {

    List<VisitedPlace> findByPlaceId(Long placeId);

    @Query(nativeQuery = true,
            value = "select * from playtour.public.visited_place as vp" +
                    " where vp.user_id = :userId")
    List<VisitedPlace> findByUserId(@Param("userId") String userId);

    @Modifying
    @Query(nativeQuery = true,
            value = "delete from playtour.public.visited_place as vp" +
                    " where vp.user_id = :userId")
    void deleteVisitedPlacesByUserId(@Param("userId") String userId);

    @Modifying
    @Query(nativeQuery = true,
            value = "delete from playtour.public.visited_place as vp" +
                    " where vp.place_id = :placeId")
    void deleteVisitedPlacesByPlaceId(@Param("placeId") Long placeId);
}
