package ru.project.playtour.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.project.playtour.entity.Tour;

import java.util.Optional;

@Repository
public interface TourRepository extends JpaRepository<Tour, Long> {

    @Modifying
    @Query(nativeQuery = true,
            value = "delete from playtour.public.place_in_tour as pit" +
                    " where pit.tour_id = :tourId and pit.place_id = :placeId")
    void deletePlace(@Param("tourId") Long tourId, @Param("placeId") Long placeId);

    @Modifying
    @Query(nativeQuery = true,
            value = "insert into playtour.public.place_in_tour(tour_id, place_id)" +
                    " values (:tourId, :placeId)")
    void addPlace(@Param("tourId") Long tourId, @Param("placeId") Long placeId);

    Optional<Tour> deleteTourById(Long id);
}
