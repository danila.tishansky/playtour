package ru.project.playtour.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.project.playtour.entity.PlacePhoto;

@Repository
public interface PlacePhotoRepository extends JpaRepository<PlacePhoto, Long> {
}
