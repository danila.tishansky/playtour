package ru.project.playtour.dto;

public enum UserAttribute {
    PHOTO("photo"), LEVEL("level"), EXPERIENCE("experience"), MONEY("money");

    private final String attribute;

    UserAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getAttribute() {
        return attribute;
    }
}
