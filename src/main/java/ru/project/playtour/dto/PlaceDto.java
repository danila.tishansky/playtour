package ru.project.playtour.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PlaceDto {
    @ToString.Include
    private Long id;
    private double latitude;
    private double longitude;
    @ToString.Include
    private String name;
    private String description;
    private String mapIcon;
    private int price;
    private int expValue;
    private int moneyValue;
    private List<PlacePhotoDto> photos;
    @JsonIgnore
    private List<TourDto> tours;
}
