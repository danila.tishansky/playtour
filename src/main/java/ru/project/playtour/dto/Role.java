package ru.project.playtour.dto;

public enum Role {
    USER, ADMIN
}
