package ru.project.playtour.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import redcollar.project.playtour.dto.*;
import ru.project.playtour.dto.PlaceDto;
import ru.project.playtour.dto.TourCompletedDto;
import ru.project.playtour.dto.TourDto;
import ru.project.playtour.dto.UserDto;
import ru.project.playtour.dto.VisitedPlaceDto;

@Aspect
@Slf4j
@Component
public class GameLoggingAspect {

    @Pointcut(value = "execution(* ru.project.playtour.service.GameService.visitPlace(..)) && args(user, place))",
            argNames = "user,place")
    public void userVisitedPlace(UserDto user, PlaceDto place) {
    }

    @Pointcut(value = "execution(* ru.project.playtour.service.GameService.completeTour(..)) && args(user, tour))",
            argNames = "user,tour")
    public void userCompletedTour(UserDto user, TourDto tour) {
    }

    @Around(value = "userVisitedPlace(user, place)", argNames = "joinPoint,user,place")
    public Object logVisit(ProceedingJoinPoint joinPoint, UserDto user, PlaceDto place) throws Throwable {
        log.info("{} visit {}\n", user, place);
        log.debug("User game characteristics: level={}, experience={}, money={}\n" +
                        "Place game characteristics: expValue={}, moneyValue={}",
                user.getLevel(), user.getExperience(), user.getMoney(),
                place.getExpValue(), place.getMoneyValue());
        var visitedPlaceDto = (VisitedPlaceDto) joinPoint.proceed();
        log.debug("{} updated game characteristics: level={}, experience={}, money={}",
                user, user.getLevel(), user.getExperience(), user.getMoney());
        return visitedPlaceDto;
    }

    @Around(value = "userCompletedTour(user, tour)", argNames = "joinPoint,user,tour")
    public Object logVisit(ProceedingJoinPoint joinPoint, UserDto user, TourDto tour) throws Throwable {
        log.info("{} complete {}\n", user, tour);
        log.debug("User game characteristics: level={}, experience={}, money={}\n" +
                        "Tour game characteristics: expValue={}, moneyValue={}",
                user.getLevel(), user.getExperience(), user.getMoney(),
                tour.getExpValue(), tour.getMoneyValue());
        var tourCompletedDto = (TourCompletedDto) joinPoint.proceed();
        log.debug("{} updated game characteristics: level={}, experience={}, money={}",
                user, user.getLevel(), user.getExperience(), user.getMoney());
        return tourCompletedDto;
    }
}

