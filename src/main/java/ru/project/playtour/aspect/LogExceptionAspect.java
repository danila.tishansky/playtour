package ru.project.playtour.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Slf4j
@Component
public class LogExceptionAspect {

    @AfterThrowing(value = "@annotation(ru.project.playtour.annotation.LogException)", throwing = "ex")
    public void log(JoinPoint joinPoint, Exception ex) {
        log.error("Target method {} from {} throwing exception, message: {}",
                joinPoint.getSignature().getName(),
                joinPoint.getSignature().getDeclaringType().getSimpleName(),
                ex.getMessage());
    }
}
