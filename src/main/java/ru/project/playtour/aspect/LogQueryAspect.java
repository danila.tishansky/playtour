package ru.project.playtour.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Aspect
@Slf4j
@Component
public class LogQueryAspect {

    @Around("@annotation(ru.project.playtour.annotation.LogQuery)")
    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
        var proceed = joinPoint.proceed();
        log.debug("Query {} from {} was execute. Args: {}. Response: {}",
                joinPoint.getSignature().getName(), joinPoint.getSignature().getDeclaringType().getSimpleName(),
                getArgs(joinPoint), proceed);
        return proceed;
    }

    private List<String> getArgs(ProceedingJoinPoint joinPoint) {
        var codeSignature = (CodeSignature) joinPoint.getSignature();
        var args = new ArrayList<String>();
        for (int i = 0; i < codeSignature.getParameterNames().length; i++) {
            args.add(codeSignature.getParameterNames()[i] + "=" + joinPoint.getArgs()[i]);
        }
        return args;
    }
}
