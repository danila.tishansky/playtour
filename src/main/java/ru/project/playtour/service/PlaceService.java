package ru.project.playtour.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.project.playtour.annotation.LogException;
import ru.project.playtour.annotation.LogQuery;
import ru.project.playtour.dto.PlaceDto;
import ru.project.playtour.exception.NotFoundException;
import ru.project.playtour.mapper.PlaceMapper;
import ru.project.playtour.repository.PlaceRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class PlaceService {
    private final PlaceRepository placeRepository;
    private final PlaceMapper placeMapper;
    private final VisitedPlaceService visitedPlaceService;

    @LogQuery
    @LogException
    public PlaceDto getPlaceById(Long id) {
        return placeMapper.toDto(placeRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Place not found by id=" + id)), PlaceMapper.CONTEXT);
    }

    @LogQuery
    public List<PlaceDto> getAllPlaces() {
        return placeMapper.toDto(placeRepository.findAll(), PlaceMapper.CONTEXT);
    }

    @LogQuery
    @LogException
    public void deletePlaceById(Long id) {
        placeRepository.deletePlaceById(id).orElseThrow(() ->
                new NotFoundException("Can't delete place by id=" + id + " because entity doesn't exist!"));
        visitedPlaceService.deleteVisitedPlacesByPlaceId(id);
    }

    @LogQuery
    public PlaceDto addPlace(PlaceDto placeDto) {
        placeRepository.save(placeMapper.toEntity(placeDto, PlaceMapper.CONTEXT));
        return placeDto;
    }

    @LogQuery
    public PlaceDto updatePlace(Long id, PlaceDto placeDto) {
        placeDto.setId(id);
        return addPlace(placeDto);
    }
}
