package ru.project.playtour.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import redcollar.project.playtour.dto.*;
import ru.project.playtour.dto.PlaceDto;
import ru.project.playtour.dto.TourCompletedDto;
import ru.project.playtour.dto.TourDto;
import ru.project.playtour.dto.UserDto;
import ru.project.playtour.dto.VisitedPlaceDto;

@RequiredArgsConstructor
@Service
public class GameService {

    private final KeycloakService keycloakService;
    private final VisitedPlaceService visitedPlaceService;
    private static final int EXP_LEVEL_UP = 100;

    //todo переделать игровую механику

    private void levelUp(UserDto user) {
        user.setLevel(user.getLevel() + 1);
    }

    private void rewardUser(UserDto user, PlaceDto place) {
        user.setMoney(user.getMoney() + place.getMoneyValue());
        user.setExperience(user.getExperience() + place.getExpValue());
        if (user.getExperience() >= user.getLevel() * EXP_LEVEL_UP) {
            levelUp(user);
        }
    }

    private void rewardUser(UserDto user, TourDto tour) {
        user.setMoney(user.getMoney() + tour.getMoneyValue());
        user.setExperience(user.getExperience() + tour.getExpValue());
        if (user.getExperience() >= user.getLevel() * EXP_LEVEL_UP) {
            levelUp(user);
        }
    }

    public VisitedPlaceDto visitPlace(UserDto user, PlaceDto place) {
        rewardUser(user, place);
        keycloakService.updateUser(user);
        return visitedPlaceService.visitPlace(place, user);
    }

    public TourCompletedDto completeTour(UserDto user, TourDto tour) {
        var tourCompletedDto = new TourCompletedDto();
        tourCompletedDto.setUser(user);
        tourCompletedDto.setTour(tour);
        rewardUser(user, tour);
        keycloakService.updateUser(user);
        return tourCompletedDto;
    }
}
