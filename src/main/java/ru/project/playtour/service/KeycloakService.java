package ru.project.playtour.service;

import lombok.RequiredArgsConstructor;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.project.playtour.dto.Role;
import ru.project.playtour.dto.UserDto;
import ru.project.playtour.mapper.UserMapper;

import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class KeycloakService {

    private final UserMapper userMapper;
    @Value("${keycloak.auth-server-url}")
    private String serverUrl;
    @Value("${keycloak.realm}")
    private String realm;
    @Value("${keycloak.resource}")
    private String clientId;
    @Value("${keycloak.credentials.secret}")
    private String clientSecret;
    @Value("${profiles.keycloak.admin.username}")
    private String adminUsername;
    @Value("${profiles.keycloak.admin.password}")
    private String adminPassword;

    @Bean
    private Keycloak getKeycloak() {
        return KeycloakBuilder.builder()
                .serverUrl(serverUrl)
                .realm(realm)
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .username(adminUsername)
                .password(adminPassword)
                .build();
    }

    @Bean
    private RealmResource getRealmResource() {
        return getKeycloak().realm(realm);
    }

    public UserDto addUser(UserDto user) {
        var realmResource = getRealmResource();
        var usersResource = realmResource.users();
        var userRepresentation = userMapper.toRepresentation(user);
        userRepresentation.setCredentials(Collections.singletonList(createPassword(user)));
        var response = usersResource.create(userRepresentation);
        var userId = getUserIdFromResponse(response);
        user.setId(userId);
        var userResource = usersResource.get(userId);
        var rolesResource = realmResource.roles();
        userResource.roles().realmLevel()
                .add(List.of(rolesResource.get(Role.USER.toString()).toRepresentation()));
        return user;
    }

    private String getUserIdFromResponse(Response response) {
        return response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");
    }

    private CredentialRepresentation createPassword(UserDto user) {
        var credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setTemporary(false);
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(user.getPassword());
        return credentialRepresentation;
    }

    public UserDto getUserById(String id) {
        var realmResource = getRealmResource();
        var users = realmResource.users();
        var userResource = users.get(id);
        var userRepresentation = userResource.toRepresentation();
        return userMapper.toDto(userRepresentation);
    }

    public UserDto getUserById(UUID id) {
        return getUserById(id.toString());
    }

    public UserDto updateUser(UserDto userDto) {
        var usersResource = getRealmResource().users();
        var userResource = usersResource.get(userDto.getId());
        userResource.update(userMapper.toRepresentation(userDto));
        return userDto;
    }

    public void deleteUserById(String id) {
        var usersResource = getRealmResource().users();
        usersResource.delete(id);
    }

    public List<UserDto> getAllUsers() {
        var usersResource = getRealmResource().users();
        return userMapper.toDto(usersResource.list());
    }

    public UserDto getAuthorizedUser() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        return getUserById(authentication.getPrincipal().toString());
    }
}
