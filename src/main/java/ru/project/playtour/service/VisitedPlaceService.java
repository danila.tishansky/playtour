package ru.project.playtour.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.project.playtour.annotation.LogException;
import ru.project.playtour.annotation.LogQuery;
import ru.project.playtour.dto.PlaceDto;
import ru.project.playtour.dto.UserDto;
import ru.project.playtour.dto.VisitedPlaceDto;
import ru.project.playtour.entity.VisitedPlaceKey;
import ru.project.playtour.exception.NotFoundException;
import ru.project.playtour.mapper.VisitedPlaceMapper;
import ru.project.playtour.repository.VisitedPlaceRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VisitedPlaceService {
    private final VisitedPlaceMapper visitedPlaceMapper;
    private final VisitedPlaceRepository repository;
    private final KeycloakService keycloakService;

    @LogQuery
    public List<UserDto> getVisitorsByPlaceId(Long placeId) {
        var visitedPlaces = visitedPlaceMapper.toDto(repository.findByPlaceId(placeId));
        return visitedPlaces.stream()
                .map(visitedPlace -> keycloakService.getUserById(visitedPlace.getUserId()))
                .collect(Collectors.toList());
    }

    @LogQuery
    public List<PlaceDto> getPlacesByUserId(String userId) {
        var visitedPlaces = visitedPlaceMapper.toDto(repository.findByUserId(userId));
        return visitedPlaces.stream()
                .map(VisitedPlaceDto::getPlace)
                .collect(Collectors.toList());
    }

    @LogQuery
    @LogException
    public VisitedPlaceDto getVisitedPlace(String userId, Long placeId) {
        var visitedPlaceKey = new VisitedPlaceKey();
        visitedPlaceKey.setPlaceId(placeId);
        visitedPlaceKey.setUserId(userId);
        return visitedPlaceMapper.toDto(repository.findById(visitedPlaceKey).orElseThrow(() ->
                new NotFoundException("User with id=" + userId + " hasn't visited place by id=" + placeId)));
    }

    @LogQuery
    public VisitedPlaceDto visitPlace(VisitedPlaceDto visitedPlaceDto) {
        var visitedPlace = visitedPlaceMapper.toEntity(visitedPlaceDto);
        if (!repository.existsById(visitedPlace.getId())) {
            repository.save(visitedPlace);
            return visitedPlaceDto;
        } else return getVisitedPlace(visitedPlaceDto.getUserId(), visitedPlaceDto.getPlace().getId());
    }

    public VisitedPlaceDto visitPlace(PlaceDto place, UserDto user) {
        var visitedPlaceDto = new VisitedPlaceDto();
        visitedPlaceDto.setUserId(user.getId());
        visitedPlaceDto.setPlace(place);
        visitedPlaceDto.setDate(LocalDate.now());
        return visitPlace(visitedPlaceDto);
    }

    @Transactional
    public void deleteVisitedPlacesByUserId(String userId) {
        repository.deleteVisitedPlacesByUserId(userId);
    }

    @Transactional
    public void deleteVisitedPlacesByPlaceId(Long placeId) {
        repository.deleteVisitedPlacesByPlaceId(placeId);
    }

}
