package ru.project.playtour.service;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.project.playtour.dto.Role;

import java.util.UUID;

@Service
public class AuthenticatedUserService {

    public boolean isOwner(UUID id) {
        var authenticatedUserId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        return authenticatedUserId.equals(id.toString()) || isAdmin();
    }

    private boolean isAdmin() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(Role.ADMIN.toString()));
    }
}
