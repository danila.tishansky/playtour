package ru.project.playtour.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.project.playtour.annotation.LogException;
import ru.project.playtour.annotation.LogQuery;
import ru.project.playtour.dto.PlaceDto;
import ru.project.playtour.dto.TourDto;
import ru.project.playtour.exception.NotFoundException;
import ru.project.playtour.mapper.TourMapper;
import ru.project.playtour.repository.TourRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class TourService {
    private final TourMapper tourMapper;
    private final TourRepository tourRepository;

    @LogQuery
    @LogException
    public TourDto getTourById(Long id) {
        return tourMapper.toDto(tourRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Tour not found by id=" + id)), TourMapper.CONTEXT);
    }

    @LogQuery
    public List<TourDto> getAllTours() {
        return tourMapper.toDto(tourRepository.findAll(), TourMapper.CONTEXT);
    }

    @LogQuery
    public TourDto addTour(TourDto tourDto) {
        tourRepository.save(tourMapper.toEntity(tourDto, TourMapper.CONTEXT));
        return tourDto;
    }

    @LogQuery
    @LogException
    public void deleteTourById(Long id) {
         tourRepository.deleteTourById(id).orElseThrow(() ->
                 new NotFoundException("Can't delete tour by id=" + id + "because entity doesn't exist!"));
    }

    @LogQuery
    public TourDto addPlaceByTourId(Long tourId, Long placeId) {
        tourRepository.addPlace(tourId, placeId);
        return getTourById(tourId);
    }

    @LogQuery
    public TourDto updateTourById(Long id, TourDto tourDto) {
        tourDto.setId(id);
        return addTour(tourDto);
    }

    @LogQuery
    public List<PlaceDto> getAllPlacesByTourId(Long id) {
        return getTourById(id).getPlaces();
    }

    @LogQuery
    public void deletePlace(Long tourId, Long placeId) {
        tourRepository.deletePlace(tourId, placeId);
    }

}
