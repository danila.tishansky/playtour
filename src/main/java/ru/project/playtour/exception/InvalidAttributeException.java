package ru.project.playtour.exception;

public class InvalidAttributeException extends RuntimeException {

    public InvalidAttributeException(String attribute) {
        super("Invalid attribute: " + attribute);
    }

    public InvalidAttributeException() {
        super("Attribute map is null");
    }
}
