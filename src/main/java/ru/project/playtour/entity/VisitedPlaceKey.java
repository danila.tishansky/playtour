package ru.project.playtour.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@EqualsAndHashCode
@Getter
@Setter
@Embeddable
public class VisitedPlaceKey implements Serializable {

    @Column(name = "user_id")
    private String userId;

    @Column(name = "place_id")
    private Long placeId;
}
