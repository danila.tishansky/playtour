package ru.project.playtour.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "place")
public class Place {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "latitude")
    private Float latitude;

    @Column(name = "longitude")
    private Float longitude;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "map_icon")
    private String mapIcon;

    @Column(name = "price")
    private int price;

    @Column(name = "exp_value")
    private int expValue;

    @Column(name = "money_value")
    private int moneyValue;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "place")
    private List<PlacePhoto> photos;

    @OneToMany(mappedBy = "place")
    private List<VisitedPlace> visitors;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "place_in_tour",
            joinColumns = @JoinColumn(name = "place_id"),
            inverseJoinColumns = @JoinColumn(name = "tour_id"))
    private List<Tour> tours;
}
