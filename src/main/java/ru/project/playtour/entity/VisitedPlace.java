package ru.project.playtour.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "visited_place", schema = "public")
public class VisitedPlace {

    @EmbeddedId
    private VisitedPlaceKey id;

    @ManyToOne
    @MapsId("placeId")
    @JoinColumn(name = "place_id")
    private Place place;

    @Column(name = "date")
    private LocalDate date;

}
