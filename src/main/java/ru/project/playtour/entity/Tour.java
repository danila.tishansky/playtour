package ru.project.playtour.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "tour")
public class Tour {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "exp_value")
    private int expValue;

    @Column(name = "money_value")
    private int moneyValue;

    @ManyToMany(mappedBy = "tours")
    private List<Place> places;
}