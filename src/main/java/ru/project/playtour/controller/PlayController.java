package ru.project.playtour.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.project.playtour.dto.TourCompletedDto;
import ru.project.playtour.dto.VisitedPlaceDto;
import ru.project.playtour.service.GameService;
import ru.project.playtour.service.KeycloakService;
import ru.project.playtour.service.PlaceService;
import ru.project.playtour.service.TourService;

import java.util.UUID;

@RestController
@RequestMapping("play")
@RequiredArgsConstructor
public class PlayController {
    private final GameService gameService;
    private final KeycloakService keycloakService;
    private final PlaceService placeService;
    private final TourService tourService;

    @PostMapping("visit-place/users/{userId}/places/{placeId}")
    @PreAuthorize("@authenticatedUserService.isOwner(#userId)")
    public VisitedPlaceDto visitPlace(@PathVariable UUID userId, @PathVariable Long placeId) {
        return gameService.visitPlace(keycloakService.getUserById(userId), placeService.getPlaceById(placeId));
    }

    @PostMapping("tour-complete/users/{userId}/tours/{tourId}")
    @PreAuthorize("@authenticatedUserService.isOwner(#userId)")
    public TourCompletedDto tourComplete(@PathVariable UUID userId, @PathVariable Long tourId) {
        return gameService.completeTour(keycloakService.getUserById(userId), tourService.getTourById(tourId));
    }
}
