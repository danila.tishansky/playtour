package ru.project.playtour.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.project.playtour.dto.PlaceDto;
import ru.project.playtour.dto.UserDto;
import ru.project.playtour.service.PlaceService;
import ru.project.playtour.service.VisitedPlaceService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("places")
public class PlaceController {
    private final PlaceService placeService;
    private final VisitedPlaceService visitedPlaceService;

    @GetMapping
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<PlaceDto> getAllPlaces() {
        return placeService.getAllPlaces();
    }

    @GetMapping("{id}")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public PlaceDto getPlaceById(@PathVariable Long id) {
        return placeService.getPlaceById(id);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public PlaceDto addPlace(@RequestBody PlaceDto placeDto) {
        return placeService.addPlace(placeDto);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public PlaceDto updatePlace(@PathVariable Long id, @RequestBody PlaceDto placeDto) {
        return placeService.updatePlace(id, placeDto);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deletePlaceById(@PathVariable Long id) {
        placeService.deletePlaceById(id);
        visitedPlaceService.deleteVisitedPlacesByPlaceId(id);
    }

    @GetMapping("{id}/visitors")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<UserDto> getVisitors(@PathVariable Long id) {
        return visitedPlaceService.getVisitorsByPlaceId(id);
    }
}
