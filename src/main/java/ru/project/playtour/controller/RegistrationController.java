package ru.project.playtour.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.project.playtour.dto.UserDto;
import ru.project.playtour.service.KeycloakService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@RestController
public class RegistrationController {
    private final KeycloakService keycloakService;

    @PostMapping("registration")
    public UserDto createUser(@RequestBody UserDto user) {
        return keycloakService.addUser(user);
    }

    @GetMapping("logout")
    public ResponseEntity<String> logout(HttpServletRequest request) throws ServletException {
        request.logout();
        return new ResponseEntity<>("You are logout", HttpStatus.OK);
    }
}
