package ru.project.playtour.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.project.playtour.dto.PlaceDto;
import ru.project.playtour.dto.UserDto;
import ru.project.playtour.service.KeycloakService;
import ru.project.playtour.service.VisitedPlaceService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("users")
public class UserController {
    private final KeycloakService keycloakService;
    private final VisitedPlaceService visitedPlaceService;

    @GetMapping
    public List<UserDto> getAllUsers() {
        return keycloakService.getAllUsers();
    }

    @GetMapping("me")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public UserDto getAuthorizedUser() {
        return keycloakService.getAuthorizedUser();
    }

    @GetMapping("{id}")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public UserDto getUser(@PathVariable String id) {
        return keycloakService.getUserById(id);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public UserDto createUser(@RequestBody UserDto userDto) {
        return keycloakService.addUser(userDto);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public UserDto updateUser(@RequestBody UserDto userDto) {
        return keycloakService.updateUser(userDto);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteUser(@PathVariable String id) {
        keycloakService.deleteUserById(id);
        visitedPlaceService.deleteVisitedPlacesByUserId(id);
    }

    @GetMapping("{id}/places")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<PlaceDto> getVisitedPlaces(@PathVariable String id) {
        return visitedPlaceService.getPlacesByUserId(id);
    }
}
