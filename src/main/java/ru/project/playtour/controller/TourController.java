package ru.project.playtour.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.project.playtour.dto.PlaceDto;
import ru.project.playtour.dto.TourDto;
import ru.project.playtour.service.TourService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("tours")
public class TourController {
    private final TourService tourService;

    @GetMapping
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<TourDto> getAllTours() {
        return tourService.getAllTours();
    }

    @GetMapping("{id}")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public TourDto getTourById(@PathVariable Long id) {
        return tourService.getTourById(id);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN')")
    public TourDto addTour(@RequestBody TourDto tourDto) {
        return tourService.addTour(tourDto);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public TourDto updateTour(@PathVariable Long id, @RequestBody TourDto tourDto) {
        return tourService.updateTourById(id, tourDto);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public void deleteTourById(@PathVariable Long id) {
        tourService.deleteTourById(id);
    }

    @GetMapping("{id}/places")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<PlaceDto> getPlacesByTourId(@PathVariable Long id) {
        return tourService.getAllPlacesByTourId(id);
    }

    @PostMapping("{tourId}/places/{placeId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public TourDto addPlace(@PathVariable Long tourId, @PathVariable Long placeId) {
        return tourService.addPlaceByTourId(tourId, placeId);
    }

    @DeleteMapping("{tourId}/places/{placeId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public void deletePlaceFromTour(@PathVariable Long tourId, @PathVariable Long placeId) {
        tourService.deletePlace(tourId, placeId);
    }
}
